import {Expose, Type} from 'class-transformer';
import 'reflect-metadata';

export namespace DataForm {

    export class Form {

        @Expose()
        private id: string | null = null;

        @Expose() @Type(() => Metadata)
        private metadata: Metadata | null = null;

        @Expose() @Type(() => Document)
        private document: Document[] | null = null;
    }

    export class Metadata {

        @Expose()
        private title: string | null = null;

        @Expose()
        private author: string | null = null;

        @Expose()
        private publisher: string | null = null;

        @Expose()
        private year: string | null = null;

        @Expose()
        private annotation_level: string[] | null = null;

        @Expose()
        private note: string | null = null;
    }

    export class Document {

        @Expose()
        private id: string | null = null;

        @Expose() @Type(() => DocumentMetadata)
        private metadata: DocumentMetadata | null = null;

        @Expose() @Type(() => DocumentSpeaker)
        private speaker: DocumentSpeaker[] | null = null;

        @Expose() @Type(() => DocumentSentence)
        private sentence: DocumentSentence[] | null = null;

        @Expose() @Type(() => DocumentCR)
        private CR: DocumentCR[] | null = null;

        @Expose() @Type(() => DocumentZA)
        private ZA: DocumentZA[] | null = null;
    }

    export class DocumentMetadata {

        @Expose()
        private title: string | null = null;

        @Expose()
        private author: string | null = null;

        @Expose()
        private publisher: string | null = null;

        @Expose()
        private url: string[] | null = null;

        @Expose()
        private date: string | null = null;

        @Expose()
        private category: string | null = null;

        @Expose()
        private note: string[] | null = null;
    }

    export class DocumentSpeaker {

        @Expose()
        private id: number | null = null;

        @Expose()
        private age: string | null = null;

        @Expose()
        private home: string | null = null;

        @Expose()
        private job: string | null = null;

        @Expose()
        private sex: string | null = null;
    }

    export class DocumentSentence {

        @Expose()
        private id: string | null = null;

        @Expose()
        private form: string | null = null;

        @Expose() @Type(() => DocumentSentenceUtterance)
        private utterance: DocumentSentenceUtterance | null = null;

        @Expose() @Type(() => DocumentSentenceWord)
        private word: DocumentSentenceWord[] | null = null;

        @Expose() @Type(() => DocumentSentenceMorpheme)
        private morpheme: DocumentSentenceMorpheme[] | null = null;

        @Expose() @Type(() => DocumentSentenceWSD)
        private WSD: DocumentSentenceWSD[] | null = null;

        @Expose() @Type(() => DocumentSentenceNE)
        private NE: DocumentSentenceNE[] | null = null;

        @Expose() @Type(() => DocumentSentenceDP)
        private DP: DocumentSentenceDP[] | null = null;

        @Expose() @Type(() => DocumentSentenceSRL)
        private SRL: DocumentSentenceSRL[] | null = null;
    }


    export class DocumentSentenceUtterance {

        @Expose()
        private id: string | null = null;

        @Expose()
        private form: string | null = null;

        @Expose()
        private speaker_id: string | null = null;

        @Expose()
        private note: string | null = null;
    }

    export class DocumentSentenceWord {

        @Expose()
        private id: number | null = null;

        @Expose()
        private form: string | null = null;

        @Expose()
        private begin: number | null = null;

        @Expose()
        private end: number | null = null;
    }

    export class DocumentSentenceMorpheme {

        @Expose()
        private id: number | null = null;

        @Expose()
        private form: string | null = null;

        @Expose()
        private label: string | null = null;

        @Expose()
        private word_id: number | null = null;

        @Expose()
        private position: number | null = null;
    }

    export class DocumentSentenceWSD {

        @Expose()
        private word: string | null = null;

        @Expose()
        private sense_id: number | null = null;

        @Expose()
        private begin: number | null = null;

        @Expose()
        private end: number | null = null;
    }

    export class DocumentSentenceNE {

        @Expose()
        private id: number | null = null;

        @Expose()
        private form: string | null = null;

        @Expose()
        private label: string | null = null;

        @Expose()
        private begin: number | null = null;

        @Expose()
        private end: number | null = null;
    }

    export class DocumentSentenceDP {

        @Expose()
        private word_id: number | null = null;

        @Expose()
        private word_form: string | null = null;

        @Expose()
        private head: number | null = null;

        @Expose()
        private label: string | null = null;

        @Expose()
        private dependent: number[] | null = null;
    }

    export class DocumentSentenceSRL {

        @Expose() @Type(() => DocumentSentenceSRLPredicate)
        private predicate: DocumentSentenceSRLPredicate | null = null;

        @Expose() @Type(() => DocumentSentenceSRLArgument)
        private argument: DocumentSentenceSRLArgument[] | null = null;
    }

    export class DocumentSentenceSRLPredicate {

        @Expose()
        private form: string | null = null;

        @Expose()
        private sense_id: number | null = null;

        @Expose()
        private word_id: number | null = null;
    }

    export class DocumentSentenceSRLArgument {

        @Expose()
        private word_id: number | null = null;

        @Expose()
        private word_form: string | null = null;

        @Expose()
        private label: string | null = null;
    }

    export class DocumentCR {

        @Expose() @Type(() => DocumentCRMention)
        private mention: DocumentCRMention[] | null = null;
    }

    export class DocumentCRMention {

        @Expose()
        private form: string | null = null;

        @Expose()
        private NE_id: number | null = null;

        @Expose()
        private sentence_id: string | null = null;

        @Expose()
        private begin: number | null = null;

        @Expose()
        private end: number | null = null;
    }

    export class DocumentZA {

        @Expose()
        private type: string | null = null;

        @Expose() @Type(() => DocumentZAPredicate)
        private predicate: DocumentZAPredicate | null = null;

        @Expose() @Type(() => DocumentZAAntecedent)
        private antecedent: DocumentZAAntecedent | null = null;

    }

    export class DocumentZAPredicate {

        @Expose()
        private word_id: number | null = null;

        @Expose()
        private word_form: number | null = null;

        @Expose()
        private sentence_id: string | null = null;

    }

    export class DocumentZAAntecedent {

        @Expose()
        private form: string | null = null;

        @Expose()
        private sentence_id: string | null = null;

        @Expose()
        private begin: number | null = null;

        @Expose()
        private end: number | null = null;
    }
}

