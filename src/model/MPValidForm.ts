import { ValidateNested, IsString, IsInt, IsNotEmpty, IsArray, ArrayNotEmpty } from 'class-validator';
import { Expose, Type } from "class-transformer";
import "reflect-metadata";

export class Msg {
    public property! : string;
    public value! : string;
    public constraints! :    string;
    public node!: string;
    public object!: object;
}

function msgConv(args:Msg){
    return JSON.stringify({ 'targetName' : '형태 분석', 'node' : args.node, 'property' : args.property, 'value' : args.value, 'constraints': args.constraints, 'object' : args.object });
}

export namespace MPValidForm {

    export class Form {

        @Expose() @ValidateNested() @Type(() => Document)
        private document: Document[] | null = null;
    }

    export class Document {

        @Expose() @ValidateNested() @Type(() => DocumentSentence)
        @ArrayNotEmpty({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        @IsArray({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        private sentence: DocumentSentence[] | null = null;
    }

    export class DocumentSentence {

        @Expose() @Type(() => DocumentSentenceMorpheme)
        @ArrayNotEmpty({message : args => { args.node = 'root -> document -> sentence ->'; return msgConv(args);}})
        @IsArray({message : args => { args.node = 'root -> document -> sentence ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence ->'; return msgConv(args);}})
        private morpheme: DocumentSentenceMorpheme[] | null = null;
    }

    export class DocumentSentenceMorpheme {

        @Expose()
        @IsInt({message : args => { args.node = 'root -> document -> sentence -> morpheme ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> morpheme ->'; return msgConv(args);}})
        private id: number | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> sentence -> morpheme ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> morpheme ->'; return msgConv(args);}})
        private form: string | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> sentence -> morpheme ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> morpheme ->'; return msgConv(args);}})
        private label: string | null = null;

        @Expose()
        @IsInt({message : args => { args.node = 'root -> document -> sentence -> morpheme ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> morpheme ->'; return msgConv(args);}})
        private word_id: number | null = null;

        @Expose()
        @IsInt({message : args => { args.node = 'root -> document -> sentence -> morpheme ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> morpheme ->'; return msgConv(args);}})
        private position: number | null = null;
    }
}


