import { ValidateNested, IsString, IsInt, IsNotEmpty, IsArray, ArrayNotEmpty } from 'class-validator';
import { Expose, Type } from "class-transformer";
import "reflect-metadata";

export class Msg {
    public property! : string;
    public value! : string;
    public constraints! :    string;
    public node!: string;
    public object!: object;
}

function msgConv(args:Msg){
    return JSON.stringify({ 'targetName' : '의미역 분석', 'node' : args.node, 'property' : args.property, 'value' : args.value, 'constraints': args.constraints, 'object' : args.object });
}

export namespace SRValidForm {

    export class Form {

        @Expose() @ValidateNested() @Type(() => Document)
        private document: Document[] | null = null;
    }

    export class Document {

        @Expose() @ValidateNested() @Type(() => DocumentSentence)
        @ArrayNotEmpty({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        @IsArray({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        private sentence: DocumentSentence[] | null = null;
    }

    export class DocumentSentence {

        @Expose() @ValidateNested() @Type(() => DocumentSentenceSRL)
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence ->'; return msgConv(args);}})
        @ArrayNotEmpty({message : args => { args.node = 'root -> document -> sentence ->'; return msgConv(args);}})
        @IsArray({message : args => { args.node = 'root -> document -> sentence ->'; return msgConv(args);}})
        private SRL: DocumentSentenceSRL[] | null = null;
    }

    export class DocumentSentenceSRL {

        @Expose() @ValidateNested() @Type(() => DocumentSentenceSRLPredicate)
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> SRL ->'; return msgConv(args);}})
        private predicate: DocumentSentenceSRLPredicate | null = null;

        @Expose() @ValidateNested() @Type(() => DocumentSentenceSRLArgument)
        @ArrayNotEmpty({message : args => { args.node = 'root -> document -> sentence -> SRL ->'; return msgConv(args);}})
        @IsArray({message : args => { args.node = 'root -> document -> sentence -> SRL ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> SRL ->'; return msgConv(args);}})
        private argument: DocumentSentenceSRLArgument[] | null = null;
    }

    export class DocumentSentenceSRLPredicate {

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> sentence -> SRL -> predicate ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> SRL -> predicate ->'; return msgConv(args);}})
        private form: string | null = null;

        @Expose()
        @IsInt({message : args => { args.node = 'root -> document -> sentence -> SRL -> predicate ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> SRL -> predicate ->'; return msgConv(args);}})
        private begin: number | null = null;

        @Expose()
        @IsInt({message : args => { args.node = 'root -> document -> sentence -> SRL -> predicate ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> SRL -> predicate ->'; return msgConv(args);}})
        private end: number | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> sentence -> SRL -> predicate ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> SRL -> predicate ->'; return msgConv(args);}})
        private lemma: string | null = null;

        @Expose()
        @IsInt({message : args => { args.node = 'root -> document -> sentence -> SRL -> predicate ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> SRL -> predicate ->'; return msgConv(args);}})
        private sense_id: number | null = null;
    }

    export class DocumentSentenceSRLArgument {

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> sentence -> SRL -> predicate ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> SRL -> predicate ->'; return msgConv(args);}})
        private form: string | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> sentence -> SRL -> argument ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> SRL -> argument ->'; return msgConv(args);}})
        private label: string | null = null;

        @Expose()
        @IsInt({message : args => { args.node = 'root -> document -> sentence -> SRL -> predicate ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> SRL -> predicate ->'; return msgConv(args);}})
        private begin: number | null = null;

        @Expose()
        @IsInt({message : args => { args.node = 'root -> document -> sentence -> SRL -> predicate ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> SRL -> predicate ->'; return msgConv(args);}})
        private end: number | null = null;
    }
}

