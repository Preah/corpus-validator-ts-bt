import { ValidateNested, IsString, IsInt, IsNotEmpty, IsArray, ArrayNotEmpty } from 'class-validator';
import { Expose, Type } from "class-transformer";
import "reflect-metadata";

export class Msg {
    public property! : string;
    public value! : string;
    public constraints! :    string;
    public node!: string;
    public object!: object;
}

function msgConv(args:Msg){
    return JSON.stringify({ 'targetName' : '구어', 'node' : args.node, 'property' : args.property, 'value' : args.value, 'constraints': args.constraints, 'object' : args.object });
}

export namespace SpeakerValidForm {

    export class SpeakerValidForm {

        @Expose() @ValidateNested() @Type(() => Document)
        private document: Document[] | null = null;
    }

    export class Document {

        @Expose() @ValidateNested() @Type(() => DocumentSpeaker)
        @ArrayNotEmpty({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        @IsArray({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        private speaker: DocumentSpeaker[] | null = null;

        @Expose() @ValidateNested() @Type(() => DocumentSpeaker)
        @ArrayNotEmpty({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        @IsArray({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        private paragraph: DocumentParagraph[] | null = null;

        @Expose() @ValidateNested() @Type(() => DocumentSentence)
        @ArrayNotEmpty({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        @IsArray({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        private sentence: DocumentSentence[] | null = null;
    }

    export class DocumentParagraph {

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> paragraph ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> paragraph ->'; return msgConv(args);}})
        private id: string | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> paragraph ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> paragraph ->'; return msgConv(args);}})
        private form: string | null = null;
    }


    export class DocumentSpeaker {

        @Expose()
        @IsInt({message : args => { args.node = 'root -> document -> speaker ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> speaker ->'; return msgConv(args);}})
        private id: number | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> speaker ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> speaker ->'; return msgConv(args);}})
        private age: string | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> speaker ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> speaker ->'; return msgConv(args);}})
        private home: string | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> speaker ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> speaker ->'; return msgConv(args);}})
        private job: string | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> speaker ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> speaker ->'; return msgConv(args);}})
        private sex: string | null = null;
    }

    export class DocumentSentence {

        @Expose() @ValidateNested() @Type(() => DocumentSentenceUtterance)
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence ->'; return msgConv(args);}})
        private utterance: DocumentSentenceUtterance | null = null;
    }

    export class DocumentSentenceUtterance {

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> sentence -> utterance ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> utterance ->'; return msgConv(args);}})
        private id: string | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> sentence -> utterance ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> utterance ->'; return msgConv(args);}})
        private form: string | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> sentence -> utterance ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> utterance ->'; return msgConv(args);}})
        private speaker_id: string | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> sentence -> utterance ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> utterance ->'; return msgConv(args);}})
        private note: string | null = null;
    }
}


