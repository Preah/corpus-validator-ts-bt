import { ValidateNested, IsString, IsInt, IsNotEmpty, IsArray, ArrayNotEmpty } from 'class-validator';
import { Expose, Type } from "class-transformer";
import "reflect-metadata";

export class Msg {
    public property!: string;
    public value!: string;
    public constraints!: string;
    public node!: string;
    public object!: object;
}

function msgConv(args:Msg){
    return JSON.stringify({ 'targetName' : '공통 형식', 'node' : args.node, 'property' : args.property, 'value' : args.value, 'constraints': args.constraints, 'object' : args.object });
}


export namespace DefaultValidForm {

    export class Form {

        @Expose()
        @IsString({message : args => { args.node = 'root ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root ->'; return msgConv(args);}})
        private id: string | null = null;

        @Expose() @ValidateNested() @Type(() => Metadata)
        @IsNotEmpty({message : args => { args.node = 'root ->'; return msgConv(args);}})
        private metadata: Metadata | null = null;

        @Expose() @ValidateNested() @Type(() => Document)
        @ArrayNotEmpty({message : args => { args.node = 'root ->'; return msgConv(args);}})
        @IsArray({message : args => { args.node = 'root ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root ->'; return msgConv(args);}})
        private document: Document[] | null = null;
    }

    export class Metadata {

        @Expose()
        @IsString({message : args => { args.node = 'root -> metadata ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> metadata ->'; return msgConv(args);}})
        private title: string | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> metadata ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> metadata ->'; return msgConv(args);}})
        private author: string | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> metadata ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> metadata ->'; return msgConv(args);}})
        private publisher: string | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> metadata ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> metadata ->'; return msgConv(args);}})
        private year: string | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> metadata ->'; return msgConv(args);}})
        private note: string | null = null;
    }

    export class Document {

        @Expose()
        @IsString({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        private id: string | null = null;

        @Expose() @ValidateNested() @Type(() => DocumentMetadata)
        @IsNotEmpty({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        private metadata: DocumentMetadata | null = null;

        @Expose() @ValidateNested() @Type(() => DocumentSentence)
        @ArrayNotEmpty({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        @IsArray({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        private sentence: DocumentSentence[] | null = null;
    }

    export class DocumentMetadata {

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> metadata ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> metadata ->'; return msgConv(args);}})
        private title: string | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> metadata ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> metadata ->'; return msgConv(args);}})
        private author: string | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> metadata ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> metadata ->'; return msgConv(args);}})
        private publisher: string | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> metadata ->'; return msgConv(args);}})
        private url: string | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> metadata ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> metadata ->'; return msgConv(args);}})
        private date: string | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> metadata ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> metadata ->'; return msgConv(args);}})
        private category: string | null = null;

        @Expose()
        @ArrayNotEmpty({message : args => { args.node = 'root -> metadata ->'; return msgConv(args);}})
        @IsArray({message : args => { args.node = 'root -> metadata ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> metadata ->'; return msgConv(args);}})
        private annotation_level: string[] | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> metadata ->'; return msgConv(args);}})
        private note: string | null = null;
    }

    export class DocumentSentence {

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> sentence ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence ->'; return msgConv(args);}})
        private id: string | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> sentence ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence ->'; return msgConv(args);}})
        private form: string | null = null;

        @Expose() @ValidateNested() @Type(() => DocumentSentenceWord)
        @ArrayNotEmpty({message : args => { args.node = 'root -> document -> sentence ->'; return msgConv(args);}})
        @IsArray({message : args => { args.node = 'root -> document -> sentence ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence ->'; return msgConv(args);}})
        private word: DocumentSentenceWord[] | null = null;
    }

    export class DocumentSentenceWord {

        @Expose()
        @IsInt({message : args => { args.node = 'root -> document -> sentence -> word ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> word ->'; return msgConv(args);}})
        private id: number | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> sentence -> word ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> word ->'; return msgConv(args);}})
        private form: string | null = null;

        @Expose()
        @IsInt({message : args => { args.node = 'root -> document -> sentence -> word ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> word ->'; return msgConv(args);}})
        private begin: number | null = null;

        @Expose()
        @IsInt({message : args => { args.node = 'root -> document -> sentence -> word ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> word ->'; return msgConv(args);}})
        private end: number | null = null;
    }
}
