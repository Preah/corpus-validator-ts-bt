import { ValidateNested, IsString, IsInt, IsNotEmpty, IsArray, ArrayNotEmpty } from 'class-validator';
import { Expose, Type } from "class-transformer";
import "reflect-metadata";

export class Msg {
    public property! : string;
    public value! : string;
    public constraints! :    string;
    public node!: string;
    public object!: object;
}

function msgConv(args:Msg){
    return JSON.stringify({ 'targetName' : '무형 대용어 복원', 'node' : args.node, 'property' : args.property, 'value' : args.value, 'constraints': args.constraints, 'object' : args.object });
}

export namespace ZAValidForm {

    export class Form {

        @Expose() @ValidateNested() @Type(() => Document)
        private document: Document[] | null = null;
    }

    export class Document {

        @Expose() @ValidateNested() @Type(() => DocumentZA)
        @ArrayNotEmpty({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        @IsArray({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        private ZA: DocumentZA[] | null = null;
    }

    export class DocumentZA {

        @Expose() @ValidateNested() @Type(() => DocumentZAPredicate)
        @IsNotEmpty({message : args => { args.node = 'root -> document -> ZA ->'; return msgConv(args);}})
        private predicate: DocumentZAPredicate | null = null;

        @Expose() @ValidateNested() @Type(() => DocumentZAAntecedent)
        @ArrayNotEmpty({message : args => { args.node = 'root -> document -> ZA ->'; return msgConv(args);}})
        @IsArray({message : args => { args.node = 'root -> document -> ZA ->'; return msgConv(args);}})
        private antecedent: DocumentZAAntecedent[] | null = null;
    }

    export class DocumentZAPredicate {

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> ZA -> predicate ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> ZA -> predicate ->'; return msgConv(args);}})
        private form: number | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> ZA -> predicate ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> ZA -> predicate ->'; return msgConv(args);}})
        private sentence_id: string | null = null;

        @Expose()
        @IsInt({message : args => { args.node = 'root -> document -> ZA -> predicate ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> ZA -> predicate ->'; return msgConv(args);}})
        private begin: number | null = null;

        @Expose()
        @IsInt({message : args => { args.node = 'root -> document -> ZA -> predicate ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> ZA -> predicate ->'; return msgConv(args);}})
        private end: number | null = null;
    }

    export class DocumentZAAntecedent {

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> ZA -> antecedent ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> ZA -> antecedent ->'; return msgConv(args);}})
        private form: string | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> ZA -> antecedent ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> ZA -> antecedent ->'; return msgConv(args);}})
        private type: string | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> ZA -> antecedent ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> ZA -> antecedent ->'; return msgConv(args);}})
        private sentence_id: string | null = null;

        @Expose()
        @IsInt({message : args => { args.node = 'root -> document -> ZA -> antecedent ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> ZA -> antecedent ->'; return msgConv(args);}})
        private begin: number | null = null;

        @Expose()
        @IsInt({message : args => { args.node = 'root -> document -> ZA -> antecedent ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> ZA -> antecedent ->'; return msgConv(args);}})
        private end: number | null = null;
    }
}



