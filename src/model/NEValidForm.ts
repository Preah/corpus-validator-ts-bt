import { ValidateNested, IsString, IsInt, IsNotEmpty, IsArray, ArrayNotEmpty } from 'class-validator';
import { Expose, Type } from "class-transformer";
import "reflect-metadata";

export class Msg {
    public property! : string;
    public value! : string;
    public constraints! :    string;
    public node!: string;
    public object!: object;
}

function msgConv(args:Msg){
    return JSON.stringify({ 'targetName' : '상호참조 해결', 'node' : args.node, 'property' : args.property, 'value' : args.value, 'constraints': args.constraints, 'object' : args.object });
}

export namespace NEValidForm {
    
    export class Form {
        @Expose() @ValidateNested() @Type(() => Document)
        private document: Document[] | null = null;
    }

    export class Document {

        @Expose() @ValidateNested() @Type(() => DocumentSentence)
        @ArrayNotEmpty({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        @IsArray({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        private sentence: DocumentSentence[] | null = null;
    }

    export class DocumentSentence {

        @Expose() @ValidateNested() @Type(() => DocumentSentenceNE)
        @ArrayNotEmpty({message : args => { args.node = 'root -> document -> sentence ->'; return msgConv(args);}})
        @IsArray({message : args => { args.node = 'root -> document -> sentence ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence ->'; return msgConv(args);}})
        private NE: DocumentSentenceNE[] | null = null;
    }

    export class DocumentSentenceNE {

        @Expose()
        @IsInt({message : args => { args.node = 'root -> document -> sentence -> NE ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> NE ->'; return msgConv(args);}})
        private id: number | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> sentence -> NE ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> NE ->'; return msgConv(args);}})
        private form: string | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> sentence -> NE ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> NE ->'; return msgConv(args);}})
        private label: string | null = null;

        @Expose()
        @IsInt({message : args => { args.node = 'root -> document -> sentence -> NE ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> NE ->'; return msgConv(args);}})
        private begin: number | null = null;

        @Expose()
        @IsInt({message : args => { args.node = 'root -> document -> sentence -> NE ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> sentence -> NE ->'; return msgConv(args);}})
        private end: number | null = null;
    }

}


