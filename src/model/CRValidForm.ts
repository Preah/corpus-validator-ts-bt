import { ValidateNested, IsString, IsInt, IsNotEmpty, IsArray, ArrayNotEmpty } from 'class-validator';
import { Expose, Type } from "class-transformer";
import "reflect-metadata";

export class Msg {
    public property! : string;
    public value! : string;
    public constraints! :    string;
    public node!: string;
    public object!: object;
}

function msgConv(args:Msg){
    return JSON.stringify({ 'targetName' : '상호참조 해결', 'node' : args.node, 'property' : args.property, 'value' : args.value, 'constraints': args.constraints, 'object' : args.object });
}

export namespace  CRValidForm {

    export class Form {

        @Expose() @ValidateNested() @Type(() => Document)
        private document: Document[] | null = null;
    }

    export class Document {

        @Expose() @ValidateNested() @Type(() => DocumentCR)
        @ArrayNotEmpty({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        @IsArray({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document ->'; return msgConv(args);}})
        private CR: DocumentCR[] | null = null;
    }

    export class DocumentCR {

        @Expose() @ValidateNested() @Type(() => DocumentCRMention)
        @ArrayNotEmpty({message : args => { args.node = 'root -> document -> CR ->'; return msgConv(args);}})
        @IsArray({message : args => { args.node = 'root -> document -> CR ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> CR ->'; return msgConv(args);}})
        private mention: DocumentCRMention[] | null = null;
    }

    export class DocumentCRMention {

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> CR -> mention ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> CR -> mention ->'; return msgConv(args);}})
        private form: string | null = null;

        @Expose()
        @IsInt({message : args => { args.node = 'root -> document -> CR -> mention ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> CR -> mention ->'; return msgConv(args);}})
        private NE_id: number | null = null;

        @Expose()
        @IsString({message : args => { args.node = 'root -> document -> CR -> mention ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> CR -> mention ->'; return msgConv(args);}})
        private sentence_id: string | null = null;

        @Expose()
        @IsInt({message : args => { args.node = 'root -> document -> CR -> mention ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> CR -> mention ->'; return msgConv(args);}})
        private begin: number | null = null;

        @Expose()
        @IsInt({message : args => { args.node = 'root -> document -> CR -> mention ->'; return msgConv(args);}})
        @IsNotEmpty({message : args => { args.node = 'root -> document -> CR -> mention ->'; return msgConv(args);}})
        private end: number | null = null;
    }
}
