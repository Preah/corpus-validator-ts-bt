import Vue from 'vue';
import Vuex from 'vuex';
import {Module, VuexModule, Mutation} from 'vuex-module-decorators';

Vue.use(Vuex);

@Module({
    name: 'Store',
    dynamic: true,
    store: new Vuex.Store({})
})

export class Store extends VuexModule {
    private jsonStr = '';

    public get getJsonStr() {
        return this.jsonStr;
    }

    @Mutation
    public setJsonStr(jsonStr: string) {
        this.jsonStr = jsonStr;
    }
}
