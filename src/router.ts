import Vue from 'vue';
import Router from 'vue-router';
import Main from './components/Main.vue';
import Login from './components/Login.vue';
import NotFound from './components/NotFound.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes : [
    {path : '/', name : 'Login', component : Login},
    {path : '/main', name : 'Main', component : Main},
    {path : '*', name : 'NotFound', component : NotFound},
  ],
});
