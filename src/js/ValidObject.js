
  let vaildObject = 
  {
    "id": "WRAK00022", 
    "metadata": {
		"title": "문어 200만 배포용", 
		"author": "홍길동",
		"publisher": "국립국어원",
		"year": "2019",
		"annotation_level": ["원시", "개체명 분석", "상호참조 해결"],
		"note": "부분 추출 - 임의추출"
	},	
    "document": [
	{
		"id": "WRAK0022.1",
		"metadata": {
			"title": "극단 신기루만화경의 '설공찬전'", 
			"author": "권재현 기자",
			"publisher": "동아일보",
			"date": "20090923",
			"category": "신문 > 전국 종합지",
			"note": "분석 예제"
        },        
        "speaker" : [{
            "id" : 1,
            "age" : "Str [1]",
            "home" : "Str [1]",
            "job" : "Str [1]",
            "sex" : "Str [1]"
        }],
		"sentence": [
			{
				"id": "WRAK0022.1.1", 
				"form": "1997년 한글소설본이 발견돼 '홍길동전'을 밀어내고 최초의 한글소설 자리를 꿰찬 '설공찬전'은 조선 중종조에 이미 금서(禁書)가 된 작품이다.", 
                "utterance" : {
                    "id" : "Str [1]",
                    "form" : "Str [1]",
                    "speaker_id" : "Str [1]",
                    "note" : "Str [1]"
                },    
                "word":	[
					{"id": 1, "form": "1997년", "begin": 1, "end": 5}, 
					{"id": 2, "form": "한글소설본이", "begin": 7, "end": 12}, 
					{"id": 3, "form": "발견돼", "begin": 13, "end": 15}, 
					{"id": 4, "form": "'홍길동전'을", "begin": 17, "end": 23},
					{"id": 5, "form": "밀어내고", "begin": 25, "end": 28},
					{"id": 6, "form": "최초의", "begin": 30, "end": 32},
					{"id": 7, "form": "한글소설", "begin": 34, "end": 37},
					{"id": 8, "form": "자리를", "begin": 38, "end": 40},
					{"id": 9, "form": "꿰찬", "begin": 42, "end": 43},
					{"id": 10, "form": "'설공찬전'은", "begin": 45, "end": 51},
					{"id": 11, "form": "조선", "begin": 52, "end": 53},
					{"id": 12, "form": "중종조에", "begin": 55, "end": 58},
					{"id": 13, "form": "이미", "begin": 60, "end": 61},
					{"id": 14, "form": "금서(禁書)가", "begin": 63, "end": 68},
					{"id": 15, "form": "된", "begin": 70, "end": 70},
					{"id": 16, "form": "작품이다.", "begin": 72, "end": 76}
				],
				"NE": [
					{"id": 1, "form": "1997년", "label": "DT", "begin": 1, "end": 5}, 
					{"id": 2, "form": "한글소설본", "label": "FD", "begin": 7, "end": 11},
					{"id": 3, "form": "홍길동전", "label": "AF", "begin": 19, "end": 22},
					{"id": 4, "form": "한글소설", "label": "FD", "begin": 35, "end": 38},
					{"id": 5, "form": "설공찬전", "label": "AF", "begin": 48, "end": 51},
					{"id": 6, "form": "조선 중종조", "label": "DT", "begin": 55, "end": 60}
			       ]
			},
			{
				"id": "WRAK0022.1.2", 
				"form": "저승세계를 소개하면서 현실 권력을 매섭게 풍자한 대목이 연산군을 몰아내고 권력을 잡은 중종 시대 권력층의 역린을 건드렸기 때문이다.",
                "utterance" : {
                    "id" : "Str [1]",
                    "form" : "Str [1]",
                    "speaker_id" : "Str [1]",
                    "note" : "Str [1]"
                },    
				"word":	[
					{"id": 1, "form": "저승세계를", "begin": 1, "end": 5}, 
					{"id": 2, "form": "소개하면서", "begin": 7, "end": 11}, 
					{"id": 3, "form": "현실", "begin": 13, "end": 14}, 
					{"id": 4, "form": "권력을", "begin": 16, "end": 18},
					{"id": 5, "form": "매섭게", "begin": 20, "end": 22},
					{"id": 6, "form": "풍자한", "begin": 24, "end": 26},
					{"id": 7, "form": "대목이", "begin": 28, "end": 30},
					{"id": 8, "form": "연산군을", "begin": 32, "end": 35},
					{"id": 9, "form": "몰아내고", "begin": 37, "end": 40},
					{"id": 10, "form": "잡은", "begin": 42, "end": 43},
					{"id": 11, "form": "중종", "begin": 45, "end": 46},
					{"id": 12, "form": "시대", "begin": 48, "end": 49},
					{"id": 13, "form": "권력층의", "begin": 51, "end": 54},
					{"id": 14, "form": "역린을", "begin": 56, "end": 58},
					{"id": 15, "form": "건드렸기", "begin": 60, "end": 63},
					{"id": 16, "form": "때문이다.", "begin": 65, "end": 69}
				],
				"NE": [
					{"id": 1, "form": "연산군", "label": "PS", "begin": 32, "end": 34}, 
					{"id": 2, "form": "중종 시대", "label": "DT", "begin": 49, "end": 53}
			       ]
			},
			{
				"id": "WRAK0022.1.3", 
				"form": "극단 '신기루만화경'의 연극 '설공찬전'(이해제 작, 연출)은 이 고전소설에서 저승세계에 대한 소개는 빼고 권력의 생리를 비판한 내용을 전면에 부각했다.", 
                "utterance" : {
                    "id" : "Str [1]",
                    "form" : "Str [1]",
                    "speaker_id" : "Str [1]",
                    "note" : "Str [1]"
                },    
				"word":	[
					{"id": 1, "form": "극단", "begin": 1, "end": 2}, 
					{"id": 2, "form": "'신기루만화경'의", "begin": 4, "end": 12}, 
					{"id": 3, "form": "연극", "begin": 14, "end": 15}, 
					{"id": 4, "form": "'설공찬전'(이해제", "begin": 17, "end": 26},
					{"id": 5, "form": "작,", "begin": 28, "end": 28},
					{"id": 6, "form": "연출)은", "begin": 30, "end": 33},
					{"id": 7, "form": "이", "begin": 35, "end": 35},
					{"id": 8, "form": "고전소설에서", "begin": 37, "end": 42},
					{"id": 9, "form": "저승세계에", "begin": 44, "end": 48},
					{"id": 10, "form": "대한", "begin": 50, "end": 41},
					{"id": 11, "form": "소개는", "begin": 53, "end": 55},
					{"id": 12, "form": "빼고", "begin": 57, "end": 58},
					{"id": 13, "form": "권력의", "begin": 60, "end": 62},
					{"id": 14, "form": "생리를", "begin": 64, "end": 66},
					{"id": 15, "form": "비판한", "begin": 68, "end": 70},
					{"id": 16, "form": "내용을", "begin": 72, "end": 74},
					{"id": 17, "form": "전면에", "begin": 76, "end": 78},
					{"id": 18, "form": "부각했다.", "begin": 80, "end": 84}

				],
				"NE": [
					{"id": 1, "form": "극단 '신기루만화경'", "label": "OG", "begin": 1, "end": 11}, 
					{"id": 2, "form": "연극 '설공찬전'", "label": "AF", "begin": 14, "end": 22},
					{"id": 3, "form": "이해제", "label": "PS", "begin": 24, "end": 26},
					{"id": 4, "form": "고전소설", "label": "FD", "begin": 38, "end": 41}
			       ]
			},
			{
				"id": "WRAK0022.1.4", 
				"form": "여기서 작품은 공침의 몸을 빌려 뮤지컬 '지킬 앤드 하이드'식의 선악 대결을 한국적 해학으로 녹여낸다.",
                "utterance" : {
                    "id" : "Str [1]",
                    "form" : "Str [1]",
                    "speaker_id" : "Str [1]",
                    "note" : "Str [1]"
                },    
				"word":	[
					{"id": 1, "form": "여기서", "begin": 1, "end": 3}, 
					{"id": 2, "form": "작품은", "begin": 5, "end": 7}, 
					{"id": 3, "form": "공침의", "begin": 9, "end": 11}, 
					{"id": 4, "form": "몸을", "begin": 13, "end": 14},
					{"id": 5, "form": "빌려", "begin": 16, "end": 17},
					{"id": 6, "form": "뮤지컬", "begin": 19, "end": 21},
					{"id": 7, "form": "'지킬", "begin": 23, "end": 25},
					{"id": 8, "form": "앤드", "begin": 27, "end": 28},
					{"id": 9, "form": "하이드'식의", "begin": 30, "end": 35},
					{"id": 10, "form": "선악", "begin": 37, "end": 38},
					{"id": 11, "form": "대결을", "begin": 40, "end": 42},
					{"id": 12, "form": "한국적", "begin": 44, "end": 46},
					{"id": 13, "form": "해학으로", "begin": 48, "end": 51},
					{"id": 14, "form": "녹여낸다.", "begin": 53, "end": 57}
				],
				"NE": [
					{"id": 1, "form": "공침", "label": "PS", "begin": 9, "end": 10}, 
                    {"id": 2, "form": "몸", "label": "AM", "begin": 13, "end": 13},
					{"id": 3, "form": "뮤지컬", "label": "FD", "begin": 19, "end": 21},
					{"id": 4, "form": "지킬 앤드 하이드", "label": "AF", "begin": 24, "end": 32}			       	       
			       ]
			},
			{
				"id": "WRAK0022.1.5", 
				"form": "당대 실세인 정익로(이장원) 대감을 구워삶아 아들에게 관직의 길을 열어주려는 공침의 아비 설충수(최재섭)는 죽은 공찬이 아들의 몸에 들어왔다는 사실을 알고도 이를 묵인한다.", 
                "utterance" : {
                    "id" : "Str [1]",
                    "form" : "Str [1]",
                    "speaker_id" : "Str [1]",
                    "note" : "Str [1]"
                },    
				"word":	[
					{"id": 1, "form": "당대", "begin": 1, "end": 2}, 
					{"id": 2, "form": "실세인", "begin": 4, "end": 6}, 
					{"id": 3, "form": "정익로(이장원)", "begin": 8, "end": 15}, 
					{"id": 4, "form": "대감을", "begin": 17, "end": 19},
					{"id": 5, "form": "구워삶아", "begin": 21, "end": 24},
					{"id": 6, "form": "아들에게", "begin": 26, "end": 29},
					{"id": 7, "form": "관직의", "begin": 31, "end": 33},
					{"id": 8, "form": "길을", "begin": 35, "end": 36},
					{"id": 9, "form": "열어주려는", "begin": 38, "end": 42},
					{"id": 10, "form": "공침의", "begin": 44, "end": 46},
					{"id": 11, "form": "아비", "begin": 48, "end": 49},
					{"id": 12, "form": "설충수(최재섭)는", "begin": 51, "end": 59},
					{"id": 13, "form": "죽은", "begin": 61, "end": 62},
					{"id": 14, "form": "공찬이", "begin": 64, "end": 66},
					{"id": 15, "form": "아들의", "begin": 68, "end": 70},
					{"id": 16, "form": "몸에", "begin": 72, "end": 73},
					{"id": 17, "form": "들어왔다는", "begin": 75, "end": 79},
					{"id": 18, "form": "사실을", "begin": 81, "end": 83},
					{"id": 19, "form": "몸에", "begin": 85, "end": 86},
					{"id": 20, "form": "들어왔다는", "begin": 88, "end": 92},
					{"id": 21, "form": "사실을", "begin": 94, "end": 96}
				],
				"NE": [
					{"id": 1, "form": "정익로", "label": "PS", "begin": 8, "end": 10}, 
					{"id": 2, "form": "이장원", "label": "PS", "begin": 12, "end": 14},
					{"id": 3, "form": "대감", "label": "CV", "begin": 17, "end": 18},
					{"id": 4, "form": "아들", "label": "CV", "begin": 26, "end": 27},
					{"id": 5, "form": "공침", "label": "PS", "begin": 44, "end": 45}, 
					{"id": 6, "form": "아비", "label": "CV", "begin": 48, "end": 49},
					{"id": 7, "form": "설충수", "label": "PS", "begin": 51, "end": 53},
					{"id": 8, "form": "최재섭", "label": "PS", "begin": 55, "end": 57},
					{"id": 9, "form": "공찬", "label": "PS", "begin": 64, "end": 65},
					{"id": 10,"form": "아들", "label": "CV", "begin": 68, "end": 69},
					{"id": 11,"form": "몸", "label": "AM", "begin": 72, "end": 72}
			       ]
			}
		
		],
		"CR": [
			{
				"mention" : [
                             {"form": "설공찬전", "NE_id": 5, "sentence_id": "WRAK0022.1.1", "begin": 48, "end": 51},
                             {"form": "이 고전소설", "NE_id": -1, "sentence_id": "WRAK0022.1.3", "begin": 36, "end": 41}
                            ]
			},
			{
				"mention" : [
                             {"form": "중종 시대", "NE_id": 2, "sentence_id": "WRAK0022.1.2", "begin": 49, "end": 53},
                             {"form": "당대", "NE_id": -1, "sentence_id": "WRAK0022.1.5", "begin": 1, "end": 2}
                            ]
			},
			{
				"mention": [
                            {"form": "연극 '설공찬전'", "NE_id": 2, "sentence_id": "WRAK0022.1.3", "begin": 14, "end": 22},
                            {"form": "작품", "NE_id": -1, "sentence_id": "WRAK0022.1.4", "begin": 5, "end": 6}
                           ]
			}

		],	
		"ZA": [
			{
				"type": "subject",
				"predicate": {"word_id": 9, "word_form": "꿰찬", "sentence_id": "WRAK0022.1.1"},
				"antecedent": {"form": "설공찬전", "sentence_id": "WRAK0022.1.1", "begin": 48, "end": 51}				
			},
			{
				"type": "subject",
				"predicate": {"word_id": 2, "word_form": "소개하면서",  "sentence_id": "WRAK0022.1.2"},
				"antecedent": {"form": "설공찬전", "sentence_id": "WRAK0022.1.1", "begin": 48, "end":51} 
			},

			{
				"type": "subject",		
				"predicate": {"word_id": 15, "word_form": "비판한", "sentence_id": "WRAK0022.1.3"},
				"antecedent": {"form": "고전소설", "sentence_id": "WRAK0022.1.3", "begin": 38, "end": 41}				
			},
			{			
				"type": "subject",				
				"predicate": {"word_id": 5, "word_form": "빌려", "sentence_id": "WRAK0022.1.4"},
				"antecedent": {"form": "고전소설", "sentence_id": "WRAK0022.1.3", "begin": 38, "end": 41}				
			}
		]	
	}    	
   ]
}

export default vaildObject